using System;
using System.IO;

namespace dotNet_labs
{
	public class Logger<T> : ILogger<T>
	{
		/// <summary>
		/// The log to file.
		/// </summary>
		private bool logToFile = false;
		/// <summary>
		/// The log to console.
		/// </summary>
		private bool logToConsole = false;
		/// <summary>
		/// The name of the file.
		/// </summary>
		private string fileName = null;
		/// <summary>
		/// The console messages alloved to write level.
		/// </summary>
		private LoggerLevels consoleMessagesLevel = 0,
		/// <summary>
		/// The file messages alloved to write level.
		/// </summary>
		fileMessagesLevel = 0;

		private delegate void WriteToConcoleMethod( string message, LoggerLevels level );
		private delegate void WriteToFileMethod( string message, string fileName, LoggerLevels level );

		private event WriteToConcoleMethod WriteToConsole; 
		private event WriteToFileMethod WriteToFile; 

		public Logger( string fileName = null, bool logToConsole = true )
		{
			this.fileName = fileName;
			this.logToFile = fileName != null;
			this.logToConsole = logToConsole;

			if( logToFile )
			{
				try
				{
					Directory.CreateDirectory(Path.GetDirectoryName(fileName));
					
					if (File.Exists(fileName))
						File.Delete(fileName);

					var fs = File.Create(fileName);
					fs.Close();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
		}

		/// <summary>
		/// Sets the writing methods.
		/// </summary>
		/// <param name="classWithWritingMethods">Class with writing methods.</param>
		public void SetWritingMethods( ILoggerWritingMethods classWithWritingMethods )
		{
			this.WriteToConsole += classWithWritingMethods.WriteToConsole;
			this.WriteToFile += classWithWritingMethods.WriteToFile;
		}

		/// <summary>
		/// Gets or sets the console messages level.
		/// </summary>
		/// <value>The console messages level.</value>
		public LoggerLevels ConsoleMessagesLevel 
		{ 
			get{ return consoleMessagesLevel; } 
			set{ consoleMessagesLevel = value; }
		}

		/// <summary>
		/// Gets or sets the file messages level.
		/// </summary>
		/// <value>The file messages level.</value>
		public LoggerLevels FileMessagesLevel
		{ 
			get{ return fileMessagesLevel; } 
			set{ fileMessagesLevel = value; }
		}

		/// <summary>
		/// Write the specified message with FATAL level.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Fatal( string message )
		{
			if( this.logToConsole ) WriteToConsole( message, LoggerLevels.FATAL );
			if( this.logToFile ) WriteToFile( message, this.fileName, LoggerLevels.FATAL );
		}

		/// <summary>
		/// Write the specified message with ERROR level.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Error( string message )
		{
			if( this.logToConsole ) WriteToConsole( message, LoggerLevels.ERROR );
			if( this.logToFile ) WriteToFile( message, this.fileName, LoggerLevels.ERROR );
		}

		/// <summary>
		/// Write the specified message with WARN level.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Warn( string message )
		{
			if( this.logToConsole ) WriteToConsole( message, LoggerLevels.WARN );
			if( this.logToFile ) WriteToFile( message, this.fileName, LoggerLevels.WARN );
		}

		/// <summary>
		/// Write the specified message with INFO level.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Info( string message )
		{
			if( this.logToConsole ) WriteToConsole( message, LoggerLevels.INFO );
			if( this.logToFile ) WriteToFile( message, this.fileName, LoggerLevels.INFO );
		}

		/// <summary>
		/// Write the specified message with DEBUG level.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Debug( string message )
		{
			if( this.logToConsole ) WriteToConsole( message, LoggerLevels.DEBUG );
			if( this.logToFile ) WriteToFile( message, this.fileName, LoggerLevels.DEBUG );
		}

		/// <summary>
		/// Write the specified message with TRACE level.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Trace( string message )
		{
			if( this.logToConsole ) WriteToConsole( message, LoggerLevels.DEBUG );
			if( this.logToFile ) WriteToFile( message, this.fileName, LoggerLevels.DEBUG );
		}

	}
}

