using System;

namespace dotNet_labs
{
	public enum LoggerLevels
	{
		FATAL,
		ERROR,
		WARN,
		INFO,
		DEBUG,
		TRACE
	}
}

