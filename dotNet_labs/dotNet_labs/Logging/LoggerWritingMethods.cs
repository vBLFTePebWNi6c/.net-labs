using System;
using System.IO;

namespace dotNet_labs
{
	public class LoggerWritingMethods : ILoggerWritingMethods
	{
		/// <summary>
		/// Writes message to console with level.
		/// </summary>
		/// <param name="message">Message.</param>
		/// <param name="level">Level.</param>
		public void WriteToConsole( string message, LoggerLevels level )
		{
			Console.WriteLine( "[{0}] -- {1} -- {2}", level.ToString( "g" ), DateTime.Today.ToString(), message );
		}

		/// <summary>
		/// Writes message to file with level.
		/// </summary>
		/// <param name="message">Message.</param>
		/// <param name="fileName">File name.</param>
		/// <param name="level">Level.</param>
		public void WriteToFile( string message, string fileName, LoggerLevels level )
		{
			using(var writer = File.AppendText(fileName))
			{
				writer.WriteLine( "[{0}] -- {1} -- {2}", level.ToString( "g" ), DateTime.Today.ToString(), message );
			}
		}
	}
}

