﻿using System;
using System.Configuration;

namespace dotNet_labs
{
	[Serializable()]
	public class ConnectionToComputerTechnicsException : System.Exception
	{
		Logger<object> logger = null;

		public ConnectionToComputerTechnicsException() : base() 
		{ 
			setupLogger(); 
			logger.Error( this.ToString() );
		}
		public ConnectionToComputerTechnicsException(string message) : base(message)
		{ 
			setupLogger(); 
			logger.Error( this.ToString() );
		}
		public ConnectionToComputerTechnicsException(string message, System.Exception inner) : base(message, inner)
		{ 
			setupLogger(); 
			logger.Error( this.ToString() );
		}

		// A constructor is needed for serialization when an
		// exception propagates from a remoting server to the client. 
		protected ConnectionToComputerTechnicsException(System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) { }

		private void setupLogger()
		{
			StartupFoldersConfigSection section = (StartupFoldersConfigSection)ConfigurationManager.GetSection( "StartupFolders" );

			if( section != null )
			{
				foreach( FolderElement item in section.FolderItems )
					if( item.FolderType == "Exception_log" )
					{
						logger = new Logger<object>( item.Path + "log.txt", false );
						logger.FileMessagesLevel = LoggerLevels.ERROR;
						logger.SetWritingMethods( new LoggerWritingMethods() );
					}
				if( logger == null )
					throw new Exception( "Can not get settings for exception\'s logger." ); 
			} else
				throw new Exception( "Can not load config file." );
		}
	}
}

