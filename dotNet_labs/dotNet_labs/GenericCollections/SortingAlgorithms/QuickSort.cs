using System;
using System.Collections;

namespace dotNet_labs
{
	public class QuickSort
	{
		/// <summary>
		/// Sort the specified array with specified comparer.
		/// </summary>
		/// <param name="comparer">Comparer.</param>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void Sort<T>( Func<object, object, int> comparer, T[] array, IProgress<double> progress )
		{
			RecursiveQuickSort( comparer, 0, array.Length - 1, 1, ref array, progress );
		}

		/// <summary>
		/// Recursive function which implement QuickSort algorithm.
		/// </summary>
		/// <param name="comparer">Comparer.</param>
		/// <param name="start_index">Start_index.</param>
		/// <param name="end_index">End_index.</param>
		/// <param name="array">Array.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		private static void RecursiveQuickSort<T>( Func<object, object, int> comparer, int start_index, int end_index, double recursion_depth, ref T[] array, IProgress<double> progress )
		{
			if( end_index - start_index < 2 )
				return;

			if (progress != null) progress.Report( ( recursion_depth / ( (double)array.Length / 2 ) ) );

			int m = ( end_index + start_index ) / 2;
			int l = start_index, r = end_index;
			while( l != r )
			{
				while( comparer( array[l], array[m] ) == -1 )
				{
					l++;
					if( l == r ) goto next_step;
					if( l == m )
						m = l + 1;
				}


				while( comparer( array[r], array[m] ) != -1 && r > m )
				{
					r--;
					if( l == r ) goto next_step;
					if( r == m )
						m = r - 1;
				}

				T tmp = array[l];
				array[l] = array[r];
				array[r] = tmp;
			}

			next_step:
			RecursiveQuickSort( comparer, start_index, m, recursion_depth + 1, ref array, progress );
			RecursiveQuickSort( comparer, m + 1, end_index, recursion_depth + 1, ref array, progress );
		}
	}
}
