using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace dotNet_labs
{
	public class MyGenericCollection<T> : ICollection<T>, ICloneable, IEnumerator<T>, IEnumerable<T>// where T : IPort //new()
	{
		/// <summary>
		/// The container.
		/// </summary>
		private T[] container = null;
		/// <summary>
		/// Is disposed variable. Needs in Dispose() imlementation.
		/// </summary>
		private bool isDisposed = false;
		/// <summary>
		/// The enumerator_index which needs in IEnumerator implementation.
		/// </summary>
		private int enumerator_index = 0;
		/// <summary>
		/// The length of array container.
		/// </summary>
		private int length = 0;

		private Action<Func<object, object, int>, T[], IProgress<double>> sort;
		private Func<object, object, int> comparer;

		public MyGenericCollection ( Action<Func<object, object, int>, T[], IProgress<double>> sort, Func<object, object, int> comparer ) 
		{ 
			container = new T[length];
			this.sort = sort;
			this.comparer = comparer;
		}

		/// <summary>
		/// Sort this instance.
		/// </summary>
		public MyGenericCollection<T> Sort()
		{
			this.sort( comparer, container, null );
			return this;
		}		

		/// <summary>
		/// Sort this instance.
		/// </summary>
		public async Task< MyGenericCollection<T> > SortAsync( IProgress<double> progress )
		{
			this.sort( comparer, container, progress );
			return this;
		}

		/// <summary>
		/// Gets or sets the <see cref="dotNet_labs.MyGenericCollection`1"/> with the specified i.
		/// </summary>
		/// <param name="i">The index.</param>
		public T this[int i]
		{
			get { return container[i]; }
			set { container[i] = value; }
		}

		//ICollection methods
		public int Count { get{ return container.Length; } }

		public bool IsReadOnly { get { return container.IsReadOnly; } }

		public void Add( T item )
		{
			var new_container = new T[length + 1];
			container.CopyTo( new_container, 0 );
			new_container[ length ] = item;
			container = new_container;
			length++;
		}

		public void Clear() 
		{
			length = 0;
			container = new T[length];
		}

		public bool Contains( T item )
		{
			foreach( var element in container )
				if( Object.Equals( item, element ) )
					return true;
			return false;
		}

		public void CopyTo( T[] array, int arrayIndex )
		{
			container.CopyTo( array, 0 );
		}

		public IEnumerator<T> GetEnumerator() 
		{ 
			for (int i = 0; i < length; i++)
				yield return container[i];
		}

		public bool Remove( T item )
		{
			int index_to_remove = 0;
			bool isFound = false;
			for( int i = 0; i < length; i++ )
				if( Object.Equals( item, container[ i ] ) )
				{
					index_to_remove = i;
					isFound = true;
					break;
				}
			if( !isFound )
				return false;

			var new_container = new T[length - 1];
			System.Array.ConstrainedCopy( container, 0, new_container, 0, index_to_remove - 1 );
			System.Array.ConstrainedCopy( container, index_to_remove + 1, new_container, index_to_remove, length - index_to_remove + 1 );
			container = new_container;
			length--;

			return true;
		}

		//ICloneable methods
		public object Clone() { return new MyGenericCollection<T>( sort, comparer ); }

		//IEnumerator mothods
		IEnumerator IEnumerable.GetEnumerator()
		{
			return container.GetEnumerator();
		}

		object IEnumerator.Current { get{ return container[enumerator_index]; } }

		T IEnumerator<T>.Current { get{ return container[enumerator_index]; } }

		public bool MoveNext()
		{
			if( enumerator_index + 1 < container.Length )
			{
				enumerator_index++;
				return true;
			} else
				return false;
		}

		public void Reset() { enumerator_index = 0; }

		public void Dispose()
		{
			if (isDisposed)
				return;
			else {
				isDisposed = true;
				return;
			}
		}

	}
}
