using System;

namespace dotNet_labs
{
	public class Scanner : ComputerTechnic
	{
		public Scanner( string name )
		{
			this.Name = name;
		}

		public override void powerOn()
		{
			isPowerOn = true;
		}

		public override void powerOff()
		{
			isPowerOn = true;
		}
	}
}

