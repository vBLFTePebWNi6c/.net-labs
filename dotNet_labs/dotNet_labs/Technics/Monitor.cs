using System;

namespace dotNet_labs
{
	public class Monitor : ComputerTechnic
	{
		public Monitor( string name )
		{
			this.Name = name;
		}

		public override void powerOn()
		{
			isPowerOn = true;
		}

		public override void powerOff()
		{
			isPowerOn = true;
		}
	}
}

