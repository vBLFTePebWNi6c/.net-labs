using System;
using System.Collections.Generic;

namespace dotNet_labs
{
	public class SystemBlock : ComputerTechnic
	{
		public SystemBlock ( string name )
		{
			this.Name = name;
		}

		/// <summary>
		/// Powers on SystemBlock.
		/// </summary>
		public override void powerOn()
		{
			this.isPowerOn = true;
			var connectionsStatus = this.checkConnections();
			foreach( var tuple in connectionsStatus )
				Console.WriteLine( "Port {0}: {1}", tuple.Item1, tuple.Item2 );
		}

		/// <summary>
		/// Powers off SystemBlock.
		/// </summary>
		public override void powerOff()
		{
			this.isPowerOn = true;
		}

		/// <summary>
		/// Checks the connections.
		/// </summary>
		/// <returns>List with ports types and their status.</returns>
		private List< Tuple< IPort, String > > checkConnections()
		{
			var result = new List< Tuple< IPort, String > >();
			foreach( var port in this.ports )
			{
				string status;
				var connectedTo = port.ConnectedPort;
				if( connectedTo != null )
				{
					status = connectedTo.Owner.Name;
					if( connectedTo.Owner.IsPowerOn )
						status += " ON";
					else
						status += " OFF";
				}
				else
					status = ComputerTechnicsPort.NULL_PORT_NAME;
				result.Add( new Tuple< IPort, String >( port, status ) );
			}

			return result;
		}
	}
}

