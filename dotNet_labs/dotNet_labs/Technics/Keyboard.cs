using System;

namespace dotNet_labs
{
	public class Keyboard : ComputerTechnic
	{
		public Keyboard( string name )
		{
			this.Name = name;
		}

		public override void powerOn()
		{
			isPowerOn = true;
		}

		public override void powerOff()
		{
			isPowerOn = true;
		}
	}
}

