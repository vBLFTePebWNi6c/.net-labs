using System;

namespace dotNet_labs
{
	public class Projector : ComputerTechnic
	{
		public Projector( string name )
		{
			this.Name = name;
		}

		public override void powerOn()
		{
			isPowerOn = true;
		}

		public override void powerOff()
		{
			isPowerOn = true;
		}
	}
}

