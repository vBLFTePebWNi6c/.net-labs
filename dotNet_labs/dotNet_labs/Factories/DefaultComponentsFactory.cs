using System;

namespace dotNet_labs
{
	/// <summary>
	/// Components factory for deafault computer components.
	/// </summary>
	public class DefaultComponentsFactory : ComponentsFactory
	{
		/// <summary>
		/// Creates the keyboard.
		/// </summary>
		/// <returns>The keyboard.</returns>
		override public Keyboard createKeyboard()
		{
			Keyboard keyboard = new Keyboard( "Default Keyboard" );
			keyboard.addPort( new USB( "USB 2.0", keyboard ) );

			return keyboard;
		}

		public Keyboard createKeyboard( string name )
		{
			Keyboard keyboard = new Keyboard( name );
			keyboard.addPort( new USB( "USB 2.0", keyboard ) );

			return keyboard;
		}

		/// <summary>
		/// Creates the monitor.
		/// </summary>
		/// <returns>The monitor.</returns>
		override public Monitor createMonitor()
		{
			Monitor monitor = new Monitor( "Default Monitor" );
			monitor.addPort( new VGA( "VGA", monitor ) );

			return monitor;
		}

		/// <summary>
		/// Creates the mouse.
		/// </summary>
		/// <returns>The mouse.</returns>
		override public Mouse createMouse()
		{
			Mouse mouse = new Mouse( "Default Mouse" );
			mouse.addPort( new USB( "USB 2.0", mouse ) );

			return mouse;
		}

		/// <summary>
		/// Creates the printer.
		/// </summary>
		/// <returns>The printer.</returns>
		override public Printer createPrinter()
		{
			Printer printer = new Printer( "Default Printer" );
			printer.addPort( new USB( "USB 2.0", printer ) );

			return printer;
		}

		/// <summary>
		/// Creates the projector.
		/// </summary>
		/// <returns>The projector.</returns>
		override public Projector createProjector()
		{
			Projector projector = new Projector( "Default Projector" );
			projector.addPort( new HDMI( "HDMI", projector ) );

			return projector;
		}

		/// <summary>
		/// Creates the scanner.
		/// </summary>
		/// <returns>The scanner.</returns>
		override public Scanner createScanner()
		{
			Scanner scanner = new Scanner( "Default Scanner" );
			scanner.addPort( new USB( "USB 2.0", scanner ) );

			return scanner;
		}

		/// <summary>
		/// Creates the system block.
		/// </summary>
		/// <returns>The system block.</returns>
		override public SystemBlock createSystemBlock()
		{
			SystemBlock sys_block = new SystemBlock( "Default SystemBlock" );
			sys_block.addPort( new USB( "USB 2.0", sys_block ) );
			sys_block.addPort( new USB( "USB 2.0", sys_block ) );
			sys_block.addPort( new USB( "USB 2.0", sys_block ) );
			sys_block.addPort( new USB( "USB 2.0", sys_block ) );
			sys_block.addPort( new USB( "USB 2.0", sys_block ) );
			sys_block.addPort( new USB( "USB 2.0", sys_block ) );

			sys_block.addPort( new VGA( "VGA", sys_block ) );
			sys_block.addPort( new HDMI( "HDMI", sys_block ) );

			return sys_block;
		}
	}
}

