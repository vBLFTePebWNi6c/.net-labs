using System;
using System.Collections;

namespace dotNet_labs
{
	public class PortComparator
	{
		/// <summary>
		/// Compare the specified x and y.
		/// </summary>
		/// <param name="x">The x object.</param>
		/// <param name="y">The y object.</param>
		public static int Compare( Object x, Object y )  {
			IPort first = x as IPort;
			IPort second = y as IPort;
			int result = 0;
			result = first.ConnectedPort != null && second.ConnectedPort == null ? 1 : result;
			result = first.ConnectedPort == null && second.ConnectedPort != null ? -1 : result;
			return result;
		}
	}
}

