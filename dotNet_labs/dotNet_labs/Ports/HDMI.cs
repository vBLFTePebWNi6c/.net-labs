using System;

namespace dotNet_labs
{
	/// <summary>
	/// HDMI port.
	/// </summary>
	public class HDMI : ComputerTechnicsPort
	{
		public HDMI( string name, ComputerTechnic owner )
		{
			this.Name = name;
			this.Owner = owner;
		}
	}
}

