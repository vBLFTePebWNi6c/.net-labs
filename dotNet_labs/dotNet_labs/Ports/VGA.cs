using System;

namespace dotNet_labs
{
	/// <summary>
	/// VGA port.
	/// </summary>
	public class VGA : ComputerTechnicsPort
	{
		public VGA( string name, ComputerTechnic owner )
		{
			this.Name = name;
			this.Owner = owner;
		}
	}
}

