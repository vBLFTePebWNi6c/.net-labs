using System;

namespace dotNet_labs
{
	/// <summary>
	/// USB port.
	/// </summary>
	public class USB : ComputerTechnicsPort
	{
		public USB( string name, IComputerTechnic owner )
		{
			this.Name = name;
			this.Owner = owner;
		}
	}
}

