﻿using System;
using System.Configuration;

namespace dotNet_labs
{
	public class StartupFoldersConfigSection : ConfigurationSection
	{
		[ConfigurationProperty( "Folders" )]
		public FoldersCollection FolderItems
		{
			get { return ( (FoldersCollection)( base[ "Folders" ] ) ); }
		}
	}
}

