using System;

namespace dotNet_labs
{
	/// <summary>
	/// Components factory abstract class.
	/// </summary>
	public abstract class ComponentsFactory
	{
		public abstract Keyboard createKeyboard();
		public abstract Monitor createMonitor();
		public abstract Mouse createMouse();
		public abstract Printer createPrinter();
		public abstract Projector createProjector();
		public abstract Scanner createScanner();
		public abstract SystemBlock createSystemBlock();
	}
}

