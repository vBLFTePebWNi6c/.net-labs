using System;

namespace labs
{
	public abstract class SortingAlgorithm
	{
		abstract static void Sort<T>( Func<object, object, int> comparer, T[] array );
	}
}

