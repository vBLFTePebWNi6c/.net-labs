using System;
using Newtonsoft.Json;

namespace dotNet_labs
{
	/// <summary>
	/// Computer technics port abstract class.
	/// </summary>
	public abstract class ComputerTechnicsPort : IPort
	{
		public static string NULL_PORT_NAME = "FREE";

		protected IComputerTechnic owner;
		protected IPort connectedTo = null;
		protected string name;

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name
		{
			get { return this.name; }
			set 
			{
				if( value.Length <= 20 )
					this.name = value; 
			}
		}

		/// <summary>
		/// Connects to another port.
		/// </summary>
		/// <returns><c>true</c>, if to was connected, <c>false</c> otherwise.</returns>
		/// <param name="port">Port.</param>
		public bool connectTo( IPort port ) {
			if( port.ConnectedPort == null && this.GetType() == port.GetType() ) 
			{
				this.ConnectedPort = port;
				port.ConnectedPort = this;
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets the owner.
		/// </summary>
		/// <value>The owner.</value>
		[JsonIgnore]
		public IComputerTechnic Owner 
		{
			get { return this.owner; }
			set { this.owner = value; }
		}

		/// <summary>
		/// Gets the connected port.
		/// </summary>
		/// <value>The connected port.</value>
		public IPort ConnectedPort 
		{
			get { return this.connectedTo; }
			set { this.connectedTo = value; }
		}

	}

}

