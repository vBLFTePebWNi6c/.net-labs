using System;
using System.Collections.Generic;

namespace dotNet_labs
{
	/// <summary>
	/// Computer technics abstract class.
	/// </summary>
	public abstract class ComputerTechnic : IComputerTechnic
	{
		protected MyGenericCollection<IPort> ports = new MyGenericCollection<IPort>( QuickSort.Sort, PortComparator.Compare );
		protected string name;
		protected bool isPowerOn;

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public string Name
		{
			get { return this.name; }
			set 
			{
				if( value.Length <= 20 )
					this.name = value; 
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is power on.
		/// </summary>
		/// <value><c>true</c> if this instance is power on; otherwise, <c>false</c>.</value>
		public bool IsPowerOn 
		{
			get{ return this.isPowerOn; }
		}

		/// <summary>
		/// Gets the ports.
		/// </summary>
		/// <returns>The ports.</returns>
		public MyGenericCollection<IPort> Ports
		{
			get{ return this.ports; }
			set{ this.ports = value; }
		}

		/// <summary>
		/// Adds the port.
		/// </summary>
		/// <param name="port">Port.</param>
		public void addPort( IPort port )
		{
			ports.Add( port );
		}

		/// <summary>
		/// Removes the port.
		/// </summary>
		/// <param name="port">Port.</param>
		public void removePort( IPort port )
		{
			ports.Remove( port );
		}

		/// <summary>
		/// Connects to another technic.
		/// </summary>
		/// <param name="technic">Technic.</param>
		public void connectTo( IComputerTechnic technic )
		{
			if( this.Ports.Count == 0 ) throw new ConnectionToComputerTechnicsException( String.Format( "Technic {0} must have at least one port.", this.GetType() ) );
			if( technic.Ports.Count == 0 ) throw new ConnectionToComputerTechnicsException( String.Format( "Technic {0} must have at least one port.", technic.GetType() ) );
			if( this.Ports.Sort()[0].ConnectedPort != null ) throw new ConnectionToComputerTechnicsException( String.Format( "Technic {0} must have at least one free port.", this.GetType() ) );
			if( this.Ports.Sort()[0].ConnectedPort != null ) throw new ConnectionToComputerTechnicsException( String.Format( "Technic {0} must have at least one free port.", technic.GetType() ) );

			foreach( var my_port in this.ports )
				foreach( var other_port in technic.Ports )
					if( my_port.connectTo( other_port ) )
					{
						//Console.WriteLine( "{0} - {1} connected.", my_port.Owner, other_port.Owner );
						return;
					}

			throw new ConnectionToComputerTechnicsException( String.Format( "Unable to connect to {0}. Free ports not found.", technic.GetType() ) );
		}

		public void disconnectFrom( IComputerTechnic technic )
		{
			foreach( var my_port in this.ports )
				if( my_port.ConnectedPort != null && my_port.ConnectedPort.Owner == technic )
				{
					my_port.ConnectedPort.ConnectedPort = null;
					my_port.ConnectedPort = null;
					return;
				}
		}

		/// <summary>
		/// Powers on technic.
		/// </summary>
		public abstract void powerOn();

		/// <summary>
		/// Powers off technic.
		/// </summary>
		public abstract void powerOff();
	}
}
