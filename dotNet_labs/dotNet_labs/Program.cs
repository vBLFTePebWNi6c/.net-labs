using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using dotNet_labs_utils;

namespace dotNet_labs
{
	class MainClass
	{
		private static void drawTextProgressBar(int progress, int total)
		{
			//draw empty progress bar
			Console.CursorLeft = 0;
			Console.Write("["); //start
			Console.CursorLeft = 32;
			Console.Write("]"); //end
			Console.CursorLeft = 1;
			float onechunk = 30.0f / total;

			//draw filled part
			int position = 1;
			for (int i = 0; i < onechunk * progress; i++)
			{
				Console.BackgroundColor = ConsoleColor.Gray;
				Console.CursorLeft = position++;
				Console.Write(" ");
			}

			//draw unfilled part
			for (int i = position; i <= 31 ; i++)
			{
				Console.BackgroundColor = ConsoleColor.Green;
				Console.CursorLeft = position++;
				Console.Write(" ");
			}

			//draw totals
			Console.CursorLeft = 35;
			Console.BackgroundColor = ConsoleColor.Black;
			Console.Write(progress.ToString() + " of " + total.ToString() + "    "); //blanks at the end remove any excess
		}

		public async static void SortingTestAsync()
		{
			Console.WriteLine( "Async sorting test:" );
			int n = 5000;
			IComputerTechnic sys_block = new SystemBlock( "Special SystemBlock" );
			IComputerTechnic[] mouses = new Mouse[n];
			var defaultComponentsFactory = new DefaultComponentsFactory();
			for( int i = 0; i < n; i++ )
			{
				sys_block.addPort( new USB( "USB 2.0", sys_block ) );
				mouses[i] = defaultComponentsFactory.createMouse();
				mouses[i].connectTo( sys_block );
			}			

			for( int i = 0; i < n; i++ )
				if( i % 2 == 0 )
					mouses[i].disconnectFrom( sys_block );

			Console.WriteLine( "Begin." );
			IProgress<double> progress = new ProgressBar();
			Task< MyGenericCollection<IPort> > sorting_ports_task = sys_block.Ports.SortAsync( progress );
			await sorting_ports_task;
			Console.WriteLine( "\nEnd." );
		}		

		public static void SortingTest( IComputerTechnic SystemBlock )
		{
			MyGenericCollection<IPort> ports = SystemBlock.Ports as MyGenericCollection<IPort>;
			Console.WriteLine();
			foreach( var port in ports )
				Console.WriteLine(port.GetType());

			ports.Sort();
			Console.WriteLine();
			foreach( var port in ports )
				Console.WriteLine(port.GetType());
		}

		public static void ExceptionsHandlingTest()
		{
			Console.WriteLine( "\nExceptions handling test." );
			var defaultComponentsFactory = new DefaultComponentsFactory();
			var newSystemBlock = defaultComponentsFactory.createSystemBlock();
			IComputerTechnic[] mouses = { defaultComponentsFactory.createMouse(),
				defaultComponentsFactory.createMouse(),
				defaultComponentsFactory.createMouse(),
				defaultComponentsFactory.createMouse(),
				defaultComponentsFactory.createMouse(),
				defaultComponentsFactory.createMouse()
			};

			foreach( IComputerTechnic mouse in mouses )
				mouse.connectTo( newSystemBlock );

			IComputerTechnic seventhMouse = defaultComponentsFactory.createMouse();

			try 
			{
				seventhMouse.connectTo( newSystemBlock );
			}
			catch( ConnectionToComputerTechnicsException ex )
			{
				Console.WriteLine( "Custom exception:" );
				Console.WriteLine( ex.ToString() );
			}
			catch( Exception ex )
			{
				Console.WriteLine( "Base exception:" );
				Console.WriteLine( ex.ToString() );
			}
		}

		public static void CreateDefaultComputer()
		{
			StartupFoldersConfigSection section = (StartupFoldersConfigSection)ConfigurationManager.GetSection( "StartupFolders" );
			Logger<object> logger = null;

			if( section != null )
			{
				foreach( FolderElement item in section.FolderItems )
					if( item.FolderType == "Normal_log" )
					{
						logger = new Logger<object>( item.Path + "log.txt" );
						logger.ConsoleMessagesLevel = LoggerLevels.TRACE;
						logger.FileMessagesLevel = LoggerLevels.TRACE;
						logger.SetWritingMethods( new LoggerWritingMethods() );
					}
				if( logger == null )
					throw new Exception( "Can not get settings for default logger." ); 
			} else
				throw new Exception( "Can not load config file." );

			var MyComputer = new Dictionary< string, ComputerTechnic >();
			var defaultComponentsFactory = new DefaultComponentsFactory();

			logger.Info( "Creating default components." );
			MyComputer["SystemBlock"] = defaultComponentsFactory.createSystemBlock();
			MyComputer["Keyboard"] = defaultComponentsFactory.createKeyboard();
			MyComputer["Monitor"] = defaultComponentsFactory.createMonitor();
			MyComputer["Mouse"] = defaultComponentsFactory.createMouse();
			MyComputer["Printer"] = defaultComponentsFactory.createPrinter();
			MyComputer["Projector"] = defaultComponentsFactory.createProjector();
			MyComputer["Scanner"] = defaultComponentsFactory.createScanner();
			logger.Info( "Successfully created." );

			logger.Info( "Connecting default components to System Block." );
			try {
				MyComputer["Keyboard"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Keyboard successfully connected." );
				MyComputer["Monitor"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Monitor successfully connected." );
				MyComputer["Mouse"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Mouse successfully connected." );
				MyComputer["Printer"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Printer successfully connected." );
				MyComputer["Projector"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Projector successfully connected." );
				MyComputer["Scanner"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Scanner successfully connected." );
			} catch( ConnectionToComputerTechnicsException ex ) {
				Console.WriteLine( ex.ToString() );
			} catch( System.Exception ex ) {
				Console.WriteLine( ex.ToString() );
			}
			logger.Info( "All technics successfully connected." );

			logger.Info( "Pushing power button." );
			MyComputer[ "SystemBlock" ].powerOn();
			logger.Info( "And again." );
			MyComputer[ "SystemBlock" ].powerOff();
		}		

		public static void ExtensionsTesting()
		{
			StartupFoldersConfigSection section = (StartupFoldersConfigSection)ConfigurationManager.GetSection( "StartupFolders" );
			Logger<object> logger = null;

			if( section != null )
			{
				foreach( FolderElement item in section.FolderItems )
					if( item.FolderType == "Normal_log" )
					{
						logger = new Logger<object>( item.Path + "log.txt" );
						logger.ConsoleMessagesLevel = LoggerLevels.TRACE;
						logger.FileMessagesLevel = LoggerLevels.TRACE;
						logger.SetWritingMethods( new LoggerWritingMethods() );
					}
				if( logger == null )
					throw new Exception( "Can not get settings for default logger." ); 
			} else
				throw new Exception( "Can not load config file." );

			var MyComputer = new Dictionary< string, ComputerTechnic >();
			var defaultComponentsFactory = new DefaultComponentsFactory();

			logger.Info( "Creating default components." );
			MyComputer["SystemBlock"] = defaultComponentsFactory.createSystemBlock();
			MyComputer["Keyboard"] = defaultComponentsFactory.createKeyboard();
			MyComputer["Monitor"] = defaultComponentsFactory.createMonitor();
			MyComputer["Mouse"] = defaultComponentsFactory.createMouse();
			MyComputer["Printer"] = defaultComponentsFactory.createPrinter();
			MyComputer["Projector"] = defaultComponentsFactory.createProjector();
			MyComputer["Scanner"] = defaultComponentsFactory.createScanner();
			logger.Info( "Successfully created." );

			logger.Info( "Connecting default components to System Block." );
			try {
				MyComputer["Keyboard"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Keyboard successfully connected." );
				MyComputer["Monitor"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Monitor successfully connected." );
				MyComputer["Mouse"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Mouse successfully connected." );
				MyComputer["Printer"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Printer successfully connected." );
				MyComputer["Projector"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Projector successfully connected." );
				MyComputer["Scanner"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Scanner successfully connected." );
			} catch( ConnectionToComputerTechnicsException ex ) {
				Console.WriteLine( ex.ToString() );
			} catch( System.Exception ex ) {
				Console.WriteLine( ex.ToString() );
			}
			logger.Info( "All technics successfully connected." );

			Console.WriteLine();
			Console.WriteLine( MyComputer["SystemBlock"].Ports.ConvertToString() );
			Console.WriteLine();
			Console.WriteLine( MyComputer["SystemBlock"].Ports.GetFreePorts().ConvertToString() );
			Console.WriteLine();
			Console.WriteLine( MyComputer["SystemBlock"].Ports.GetNonFreePorts().ConvertToString() );
			Console.WriteLine( Utils.GetNonFreePorts( MyComputer["SystemBlock"].Ports.GetNonFreePorts() ).ConvertToString() );
		}

		public static void Main (string[] args)
		{
			//CreateDefaultComputer();
			//logger.Info( "Testing sorting algorithm." );
			//MyComputer[ "SystemBlock" ].Ports.Sort();
			//logger.Info( "Pushing power button for port status." );
			//MyComputer[ "SystemBlock" ].powerOn();

			//covariance
			//IEnumerable<Scanner> scanners_derived = new MyGenericCollection<Scanner>( null, null );
			//IEnumerable<ComputerTechnic> scanners_base = scanners_derived;

			//TODO: contravariance
			//IEnumerable<Scanner> scanners_base = new MyGenericCollection<ComputerTechnic>( null, null );
			//IEnumerable<Scanner> scanners_derived = scanners_base;

			//SortingTest( MyComputer["SystemBlock"] );
			//ExceptionsHandlingTest();
			//SortingTestAsync();

			ExtensionsTesting();

		}
	}
}