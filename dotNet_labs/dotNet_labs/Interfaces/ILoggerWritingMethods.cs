using System;

namespace dotNet_labs
{
	public interface ILoggerWritingMethods
	{
		void WriteToConsole( string message, LoggerLevels level );
		void WriteToFile( string message, string fileName, LoggerLevels level );
	}
}

