using System;
using System.Collections.Generic;

namespace dotNet_labs
{
	public interface IComputerTechnic
	{
		string Name { get; set; }
		bool IsPowerOn{ get; }
		void addPort( IPort port );
		MyGenericCollection<IPort> Ports{ get; set; }
		void removePort( IPort port );
		void connectTo( IComputerTechnic technic );
		void disconnectFrom( IComputerTechnic technic );
		void powerOn();
		void powerOff();
	}
}
