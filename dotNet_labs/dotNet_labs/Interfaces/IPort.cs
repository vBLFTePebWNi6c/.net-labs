using System;

namespace dotNet_labs
{
	public interface IPort
	{
		string Name { get; set; }
		IComputerTechnic Owner { get; set; }
		IPort ConnectedPort { get; set; }
		bool connectTo( IPort port );
		
	}
}

