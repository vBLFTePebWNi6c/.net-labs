using System;
using System.Collections;

namespace labs
{
	public interface ISortingAlgorithm
	{
		static void Sort( Func<object, object, bool> comparer, ref ICollection collection );
	}
}

