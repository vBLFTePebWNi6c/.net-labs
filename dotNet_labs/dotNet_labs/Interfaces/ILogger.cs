using System;

namespace dotNet_labs
{
	public interface ILogger<T>
	{
		void Fatal( string message );
		void Error( string message );
		void Warn( string message );
		void Info( string message );
		void Debug( string message );
		void Trace( string message );
		void SetWritingMethods( ILoggerWritingMethods classWithWritingMethods ); 
		LoggerLevels ConsoleMessagesLevel { get; set; }
		LoggerLevels FileMessagesLevel { get; set; }
	}
}

