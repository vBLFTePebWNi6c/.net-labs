﻿using System;
using System.Configuration;
using dotNet_labs;
using Newtonsoft.Json;

namespace dotNet_labs_utils
{
	public static class Utils
	{
		public static string ConvertToString( this MyGenericCollection<IPort> collection )
		{
			setupLogger().Info( "Conveting to string." );

			return JsonConvert.SerializeObject( collection, Formatting.None, 
				new JsonSerializerSettings { 
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore
			});
		}

		public static MyGenericCollection<IPort> GetFreePorts( this MyGenericCollection<IPort> collection )
		{
			setupLogger().Info( "Searching for free ports." );
			MyGenericCollection<IPort> result = (MyGenericCollection<IPort>)collection.Clone();
			result.Clear();
			foreach( var port in collection.Sort() )
				if( port.ConnectedPort == null )
					result.Add( port );
				else
					break;
			return result;
		}

		public static MyGenericCollection<IPort> GetNonFreePorts( this MyGenericCollection<IPort> collection )
		{
			setupLogger().Info( "Searching for non free ports." );
			MyGenericCollection<IPort> result = (MyGenericCollection<IPort>)collection.Clone();
			result.Clear();
			foreach( var port in collection.Sort() )
				if( port.ConnectedPort != null )
					result.Add( port );
			return result;
		}

		private static Logger<object> setupLogger()
		{
			StartupFoldersConfigSection section = (StartupFoldersConfigSection)ConfigurationManager.GetSection( "StartupFolders" );
			Logger<object> logger = null;

			if( section != null )
			{
				foreach( FolderElement item in section.FolderItems )
					if( item.FolderType == "Extensions_log" )
					{
						logger = new Logger<object>( item.Path + "log.txt" );
						logger.FileMessagesLevel = LoggerLevels.ERROR;
						logger.SetWritingMethods( new LoggerWritingMethods() );
					}
				if( logger == null )
					throw new Exception( "Can not get settings for exception\'s logger." ); 
			} else
				throw new Exception( "Can not load config file." );

			return logger;
		}
	}
}

