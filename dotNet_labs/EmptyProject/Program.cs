﻿using System;
using System.Collections.Generic;
using System.Configuration;
using dotNet_labs;

namespace EmptyProject
{
	class MainClass
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name="args">The command-line arguments.</param>
		public static void Main (string[] args)
		{
			StartupFoldersConfigSection section = (StartupFoldersConfigSection)ConfigurationManager.GetSection( "StartupFolders" );
			Logger<object> logger = null;

			if( section != null )
			{
				foreach( FolderElement item in section.FolderItems )
					if( item.FolderType == "Normal_log" )
					{
						logger = new Logger<object>( item.Path + "log.txt" );
						logger.ConsoleMessagesLevel = LoggerLevels.TRACE;
						logger.FileMessagesLevel = LoggerLevels.TRACE;
						logger.SetWritingMethods( new LoggerWritingMethods() );
					}
				if( logger == null )
					throw new Exception( "Can not get settings for default logger." ); 
			} else
				throw new Exception( "Can not load config file." );

			var MyComputer = new Dictionary< string, ComputerTechnic >();
			var defaultComponentsFactory = new DefaultComponentsFactory();

			logger.Info( "Creating default components." );
			MyComputer["SystemBlock"] = defaultComponentsFactory.createSystemBlock();
			MyComputer["Keyboard"] = defaultComponentsFactory.createKeyboard();
			MyComputer["Monitor"] = defaultComponentsFactory.createMonitor();
			MyComputer["Mouse"] = defaultComponentsFactory.createMouse();
			MyComputer["Printer"] = defaultComponentsFactory.createPrinter();
			MyComputer["Projector"] = defaultComponentsFactory.createProjector();
			MyComputer["Scanner"] = defaultComponentsFactory.createScanner();
			logger.Info( "Successfully created." );

			logger.Info( "Connecting default components to System Block." );
			try {
				MyComputer["Keyboard"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Keyboard successfully connected." );
				MyComputer["Monitor"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Monitor successfully connected." );
				MyComputer["Mouse"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Mouse successfully connected." );
				MyComputer["Printer"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Printer successfully connected." );
				MyComputer["Projector"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Projector successfully connected." );
				MyComputer["Scanner"].connectTo( MyComputer["SystemBlock"] );
				logger.Info( "Scanner successfully connected." );
			} catch( ConnectionToComputerTechnicsException ex ) {
				Console.WriteLine( ex.ToString() );
			} catch( System.Exception ex ) {
				Console.WriteLine( ex.ToString() );
			}
			logger.Info( "All technics successfully connected." );
		}
	}
}
